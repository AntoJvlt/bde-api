const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      DB = require('./src/models/DB'),
      StatusRouter = require('./src/routers/StatusRouter'),
      CentersRouter = require('./src/routers/CentersRouter'),
      UsersRouter = require('./src/routers/UsersRouter'),
      StatusModel = require('./src/models/Status'),
      CentersModel = require('./src/models/Center'),
      UsersModel = require('./src/models/User'),
      UsersController = require('./src/controllers/UsersController'),
      StatusController = require('./src/controllers/StatusController'),
      CentersController = require('./src/controllers/CentersController'),
      AuthController = require('./src/controllers/AuthController');

var db = new DB();

var statusModel = StatusModel(db.getSequelizeInstance());
var centersModel = CentersModel(db.getSequelizeInstance());
var usersModel = UsersModel(db.getSequelizeInstance(), statusModel, centersModel);

var statusController = new StatusController(statusModel);
var centersController = new CentersController(centersModel);
var usersController = new UsersController(usersModel);

var usersRouter = new UsersRouter(usersController);
var statusRouter = new StatusRouter(statusController);
var centersRouter = new CentersRouter(centersController);

app.use(bodyParser.json());
app.use('/auth', AuthController.router);
app.use('/users', AuthController.authValidation, usersRouter.getRouter());
app.use('/status', AuthController.authValidation, statusRouter.getRouter());
app.use('/centers', AuthController.authValidation, centersRouter.getRouter());

app.listen(3000);