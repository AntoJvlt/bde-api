const Router = require('./Router');

module.exports = class UsersRouter extends Router
{
    constructor(Controller)
    {
        super(Controller);
        this.setupRouter();
    }

    setupRouter = () =>
    {
        //Get a specific user from email
        this.router.get('/email/:email', (req, res) =>
        {
            this.controller.requestOneByEmail(req, res);
        });
    }
}