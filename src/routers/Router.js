const express = require('express');

var controller;

module.exports = class Router
{
    router;
    controller;

    constructor(Controller)
    {
        this.router = express.Router();
        this.controller = Controller;
        this.setupRouter();
    }

    setupRouter = () =>
    {
        //Get all the elements
        this.router.get('/', (req, res) => 
        {
            this.controller.requestAll(req, res);
        });

        //Get a specific elements from id
        this.router.get('/:id', (req, res) =>
        {
            this.controller.requestOneById(req, res);
        });

        //Delete a specific elements from id
        this.router.delete('/:id', (req, res) =>
        {
            this.controller.deleteOne(req, res);
        });

        //Create a specific elements by passing a json object
        this.router.post('/', (req, res) =>
        {   
            this.controller.createOne(req, res);
        });

        //Upadate data for a specific elements by passing a json object
        this.router.put('/:id', (req, res) =>
        {
            this.controller.updateOne(req, res);
        });
    }

    getRouter = () =>
    {
        return this.router;
    }
}