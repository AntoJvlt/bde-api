const Controller = require('./Controller');
const ErrorListener = require('../models/ErrorListener');
const ErrorFormat = require('../models/ErrorFormat');

module.exports = class UsersController extends Controller
{
    constructor(Model)
    {
        super(Model);
    }

    //Response by sending one user with a specific email, in json format.
    requestOneByEmail = (req, res) =>
    {
        var email = req.params.email;

        if(typeof email === 'string')
        {
            this.model.findOne({ where: { email: email } }).then(result => 
            {
                if(result)
                    res.status(200).json(this.addLinks(result));
                else
                    res.status(404).json(ErrorFormat(404, 'RESSOURCE_NOT_FOUND', 'Ressource with Email ' + email + ' was not found.'));
            })
        }else
            res.status(400).json(ErrorFormat(400, 'INVALID_PARAMETER', 'Email should be a String'));
    }

    //Add links of navigation, for the principle of HATEOAS
    addLinks = (user) =>
    {
        user.dataValues.links = 
        {
            self: "http:\/\/localhost:3000\/users\/" + user.id,
            status: "http:\/\/localhost:3000\/status\/" + user.id_Status,
            center: "http:\/\/localhost:3000\/centers\/" + user.id_Centers,
        }

        return user;
    }

    //Override bodyDataValidation from Controller, used when creating or updating a user
    //Handling validation of body data
    bodyDataValidation = (body) =>
    {
        if(!body)
            throw new ErrorListener.RequestBodyException();
        else if(!body.hasOwnProperty('firstName'))
            throw new ErrorListener.BodyPropertyException('firstName');
        else if(!body.hasOwnProperty('lastName'))
            throw new ErrorListener.BodyPropertyException('lastName');
        else if(!body.hasOwnProperty('email'))
            throw new ErrorListener.BodyPropertyException('email');
        else if(!body.hasOwnProperty('id_Status'))
            throw new ErrorListener.BodyPropertyException('id_Status');
        else if(!body.hasOwnProperty('id_Centers'))
            throw new ErrorListener.BodyPropertyException('id_Centers');
        else if(isNaN(body.id_Status))
            throw new ErrorListener.ValueTypeException('id_Status', 'Integer');
        else if(isNaN(body.id_Centers))
            throw new ErrorListener.ValueTypeException('id_Centers', 'Integer');
        else if(typeof body.firstName !== "string")
            throw new ErrorListener.ValueTypeException('firstName', 'String');
        else if(typeof body.lastName !== "string")
            throw new ErrorListener.ValueTypeException('lastName', 'String');
        else if(typeof body.email !== "string")
            throw new ErrorListener.ValueTypeException('email', 'String');
        else if(typeof body.id_Status !== "number")
            throw new ErrorListener.ValueTypeException('id_Status', 'Number');
        else if(typeof body.id_Centers !== "number")
            throw new ErrorListener.ValueTypeException('id_Centers', 'Number');
        else if(body.firstName.length > 20)
            throw new ErrorListener.ValueLengthException('firstName', 20, body.firstName.length);
        else if(body.lastName.length > 20)
            throw new ErrorListener.ValueLengthException('lastName', 20, body.lastName.length);
        else if(body.password.length > 100)
            throw new ErrorListener.ValueLengthException('password', 100, body.password.length);
        else if(body.email.length > 50)
            throw new ErrorListener.ValueLengthException('email', 50, body.email.length);
    }
}