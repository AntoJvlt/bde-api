const ErrorFormat = require('../models/ErrorFormat');

module.exports = class Controller
{
    model = null;

    constructor(Model)
    {
        this.model = Model;
    }

    //Response by sending all elements, in json format.
    requestAll = (req, res) =>
    {
        this.model.findAll().then(result => res.status(200).json(result));
    }

    //Response by sending one element with a specific id, in json format.
    requestOneById = (req, res) =>
    {
        var id = req.params.id;

        if(!isNaN(id))
        {
            this.model.findByPk(id).then(result => 
            {
                if(result)
                    res.status(200).json(result);
                else
                    res.status(404).json(ErrorFormat(404, 'RESSOURCE_NOT_FOUND', 'Ressource with ID ' + id + ' was not found.'));
            })
        }else
            res.status(400).json(ErrorFormat(400, 'INVALID_PARAMETER', 'ID should be an Integer.'));
    }

    //Response by deleting one element with a specific id.
    deleteOne = (req, res) =>
    {
        var id = req.params.id;

        if(!isNaN(id))
        {
            this.model.findByPk(id).then(result =>
            {
                if(result)
                    result.destroy().then(result => res.status(200).json(result)); //remove from the dataBase
                else
                    res.status(404).json(ErrorFormat(404, 'RESSOURCE_NOT_FOUND', 'Ressource with ID ' + id + ' was not found.'));
            })
        }else
            res.status(400).json(ErrorFormat(400, 'INVALID_PARAMETER', 'ID should be an Integer.'));
    }

    createOne = (req, res) =>
    {
        var body = req.body;

        try
        {
            //Error handling
            this.bodyDataValidation(body);
            
            //No exception thrown, proceed the request
            this.model.create(body).then(result => res.status(200).json(result)).catch(function(err){ res.status(500).json(err) });
        }catch(e)
        {
            res.status(400).json(ErrorFormat(400, e.errorType, e.detail));
        }
    }

    updateOne = (req, res) =>
    {
        var id = req.params.id;
        var body = req.body;

        if(!isNaN(id))
        {
            this.model.findByPk(id).then(result =>
            {
                if(result)
                {
                    try
                    {
                        //Error handling
                        this.bodyDataValidation(body);
                        //No exception thrown, proceed the request
                        this.model.update(body, {where: {id: req.params.id}}).then(() => this.model.findByPk(req.params.id).then(result => res.status(200).json(result)));
                    }catch(e)
                    {
                        res.status(400).json(ErrorFormat(400, e.errorType, e.detail));
                    }
                }else
                    res.status(404).json(ErrorFormat(404, 'RESSOURCE_NOT_FOUND', 'Ressource with ID ' + id + ' was not found.'));
            })
        }else
            res.status(400).json(ErrorFormat(400, 'INVALID_PARAMETER', 'ID should be an Integer.'));
    }

    //Handling validation of body data
    bodyDataValidation = (body) =>
    {
        throw new Error('bodyDataValidation should be implemented');
    }
}