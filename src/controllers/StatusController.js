const Controller = require('./Controller');
const ErrorListener = require('../models/ErrorListener');
const ErrorFormat = require('../models/ErrorFormat');

module.exports = class StatusController extends Controller
{
    constructor(Model)
    {
        super(Model);
    }

    //Response by sending one status with a specific name, in json format.
    requestOneByName = (req, res) =>
    {
        var name = req.params.name;

        if(typeof name === 'string')
        {
            this.model.findOne({ where: { name: name } }).then(result => 
            {
                if(result)
                    res.status(200).json(result);
                else
                    res.status(404).json(ErrorFormat(404, 'RESSOURCE_NOT_FOUND', 'Ressource with Name ' + name + ' was not found.'));
            })
        }else
            res.status(400).json(ErrorFormat(400, 'INVALID_PARAMETER', 'Name should be a String'));
    }

    //Override bodyDataValidation from Controller, used when creating or updating a status
    //Handling validation of body data
    bodyDataValidation = (body) =>
    {
        if(!body)
            throw new ErrorListener.RequestBodyException();
        else if(!body.hasOwnProperty('name'))
            throw new ErrorListener.BodyPropertyException('name');
        else if(typeof body.name !== "string")
            throw new ErrorListener.ValueTypeException('name', 'String');
        else if(body.name.length > 20)
            throw new ErrorListener.ValueLengthException('name', 20, body.name.length);
    }
}