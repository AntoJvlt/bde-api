const express = require('express'),
      router = express.Router(),
      jwt = require('jsonwebtoken'),
      config = require('../../config');

//Send token to the client if the credentials are validated
router.post('/login', (req, res) =>
{
    if(req.body.credentials)
    {
        var mail = req.body.credentials.mail;
        var pass = req.body.credentials.password;

        if(mail === config.mail && pass === config.pass)
        {
            var token = jwt.sign({ mail: mail }, config.secretKey, {
                expiresIn: 86400 //24 hours before expiration
            });

            res.status(200).send({ auth: true, token: token });
        }else
            res.status(401).send({ auth: false, message: 'Wrong credentials.' })
    }else
        res.status(401).send({ auth: false, message: 'No credentials provided.' })
});


//Verify that the given token is right
var authValidation = (req, res, next) =>
{
    var token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    
    jwt.verify(token, config.secretKey, function(err, decoded) {
      if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
      
      next();
    });
};

module.exports = {
    router: router,
    authValidation: authValidation
}