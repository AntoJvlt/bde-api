//Exception if the body is empty
function RequestBodyException()
{
    this.errorType = 'REQUEST_BODY_REQUIRED';
    this.detail = 'Request body is needed.';
};

//Exception if a property is missing
function BodyPropertyException(property)
{
    this.errorType = 'BODY_PROPERTY_REQUIRED';
    this.detail = 'Property "' + property + '" needed in the request body.';
};

//Exception if a value type is not right
function ValueTypeException(property, typeRequired)
{
    this.errorType = 'WRONG_VALUE_TYPE';
    this.detail = 'Value of "' + property + '" should be of type ' + typeRequired;
};

//Exception if a value length is not right
function ValueLengthException(property, maxSize, actualSize)
{
    this.errorType = 'WRONG_VALUE_LENGTH';
    this.detail = 'Max length for "' + property + '" value is ' + maxSize + '. Detected size is ' + actualSize;
};

module.exports = { RequestBodyException, BodyPropertyException, ValueTypeException, ValueLengthException };