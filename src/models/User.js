const Sequelize = require('Sequelize');

//Mapping the user table
module.exports = (sequelize, status, centers) => 
{
    return sequelize.define('users', 
    {
        id: 
        {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        password: Sequelize.STRING,
        email: Sequelize.STRING,
        id_Status: {
            type: Sequelize.INTEGER,
            references: {
                model: status,
                key: 'id'
            }
        }, 
        id_Centers: {
            type: Sequelize.INTEGER,
            references: {
                model: centers,
                key: 'id'
            }
        }
    }, {timestamps: false, freezeTableName: true});
}