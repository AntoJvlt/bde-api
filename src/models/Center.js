//Mapping the centers table
const Sequelize = require('Sequelize');

module.exports = (sequelize) => 
{
    return sequelize.define('centers', 
    {
        id: 
        {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: Sequelize.STRING
    }, {timestamps: false, freezeTableName: true});
}