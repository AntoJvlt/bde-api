const Sequelize = require('sequelize');

module.exports = class DB
{
    sequelize = null;

    constructor()
    {
        this.connect();
    }

    //Connect to the dataBase
    connect = () => 
    {
        this.sequelize = new Sequelize('db_bde_api', 'root', '', 
        {
            host: 'localhost',
            dialect: 'mysql'
        });

        this.sequelize.authenticate().then(function(err) 
        {
            console.log('DB Connected.');
        }).catch(function (err) 
        {
            console.log('Unable to connect to the database :', err);
        });
    }

    getSequelizeInstance = () =>
    {
        return this.sequelize;
    }
}
