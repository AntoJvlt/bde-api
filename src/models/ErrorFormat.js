//Format error data in one object, used to be sent in json
module.exports = (code, type, detail) =>
{
    return {
        error: {
            type: type,
            code: code,
            detail: detail
        }
    }
}